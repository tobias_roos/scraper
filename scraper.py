import requests
import pandas as pd
from bs4 import BeautifulSoup
from pandas import ExcelWriter

# use Excel writer 
writer = pd.ExcelWriter("output.xlsx")


#making a function to scrape multiple websites 

list_url = ["https://zoek.officielebekendmakingen.nl/kst-35570-2.html"]
def get_multi_soup(list_url):
    list_multi = []
    for url in list_url:
        soup = BeautifulSoup(requests.get(url).text, "html.parser")
        list_multi.append(soup)
    return list_multi

list_soup = get_multi_soup(list_url)     

#empty lists of titles and dataframes
caption_list = []
df_list = []
source = []
#filling empty lists of titles and dataframes with titles and dataframes based on soup
for soup in list_soup:
    for i, table in enumerate(soup.find_all("table", class_="kio2 portrait"), 1):
        df = pd.read_html(str(table))[0]
        df_list.append(df)
        caption = table.get("summary", "").replace(":", "").strip()
        source.append(soup)
        caption = caption.replace("Tabel", "")
        caption = caption.lower()
        #if len(caption) > 30:
          #  caption = caption[0:30]
       # else:
         #   caption = caption
        if not caption:
            caption = f"table {i}"
            
        print(caption)
        caption_list.append(caption)
    
    #making dataframe of caption list and df_list 

data = [caption_list, df_list]
df= pd.DataFrame(data, index= ['caption', 'dataframe']).T

#function to insert multiple strings

def search_df(strings):
    df_out = df.loc[df.caption.str.contains('|'.join(strings),regex=True)]
    return df_out

strings = ["budgettaire", "ontwikkeling"]
output = search_df(strings)

output.to_excel(writer)
writer.save()